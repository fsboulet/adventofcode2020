<?php

namespace Day12;

class Ship {
  
  public int $x;
  public int $y;
  public int $deg;
  // Waypoint
  public int $wX;
  public int $wY;
  
  public const MOVE_ACTIONS = ['N', 'S', 'E', 'W'];
  public const DIRECTIONS = [
    0 => 'E',
    90 => 'S',
    180 => 'W',
    270 => 'N',
  ];
  
  public function __construct() {
    $this->x = 0;
    $this->y = 0;
    $this->deg = 0;
    $this->wX = $this->x + 10;
    $this->wY = $this->y - 1;
  }
  
  public function action(string $action, int $value) {
    if ($action === 'F' || in_array($action, self::MOVE_ACTIONS, TRUE)) {
      $this->move($action, $value);
    }
    else {
      $this->turn($action, $value);
    }
  }
  
  public function actionWithWaypoint(string $action, int $value) {
    if ($action === 'F') {
      $this->moveToWaypoint($value);
    }
    elseif (in_array($action, self::MOVE_ACTIONS, TRUE)) {
      $this->moveWaypoint($action, $value);
    }
    else {
      $this->rotateWaypoint($action, $value);
    }
  }
  
  public function move(string $action, int $value, bool $waypoint = FALSE) {
    $direction = $action === 'F' ? $this->getDirection() : $action;
    $modifier = $direction === 'N' || $direction === 'W' ? -1 : 1;
    $coord = $direction === 'N' || $direction === 'S' ? 'y' : 'x';
    if ($waypoint) {
      $coord = 'w' . strtoupper($coord);
    }
    $this->{$coord} += $value * $modifier;
  }
  
  public function turn(string $action, int $value) {
    $value %= 360;
    if ($action === 'L') {
      $value = 360 - $value;
    }
    $this->deg = ($this->deg + $value) % 360;
  }
  
  public function getDirection(): string {
    assert(isset(self::DIRECTIONS[$this->deg]), sprintf('Impossible de récupérer la direction pour l\'orientation : %s', $this->deg));
    return self::DIRECTIONS[$this->deg];
  }
  
  public function moveToWaypoint(int $multiplicator) {
    [$xVec, $yVec] = $this->getWaypointVector();
    $this->x += ($xVec * $multiplicator);
    $this->y += ($yVec * $multiplicator);
    $this->wX = $this->x + $xVec;
    $this->wY = $this->y + $yVec;
  }
  
  public function moveWaypoint(string $action, int $value) {
    $this->move($action, $value, TRUE);
  }
  
  public function rotateWaypoint(string $action, int $value) {
    $value %= 360;
    if ($action === 'L') {
      $value = 360 - $value;
    }
    $rotate90 = $value / 90;
    for ($i = 0; $i < $rotate90; $i++) {
      [$xVec, $yVec] = $this->getWaypointVector();
      $this->wX = $this->x + ($yVec * -1);
      $this->wY = $this->y + $xVec;
    }
  }
  
  public function getWaypointVector(): array {
    return [
      $this->wX - $this->x,
      $this->wY - $this->y,
    ];
  }
  
  public function __toString(): string {
    return sprintf("x: %s, y: %s, deg: %s\nwaypoint : %s / %s", $this->x, $this->y, $this->deg, $this->wX, $this->wY);
  }
  
}