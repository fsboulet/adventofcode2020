<?php

use Day12\Ship;

main(__DIR__ . '/example.txt');
main(__DIR__ . '/input.txt');

function main(string $filepath) {
  println('Input : ' . $filepath);
  $input = file_get_contents($filepath);
  $actions = [];
  if (preg_match_all('/^([NSEWLRF]+)([\d]+)\s*$/m', $input, $matches, PREG_SET_ORDER)) {
    $actions = array_map(function ($match) {
      return [
        'name' => $match[1],
        'value' => (int) $match[2],
      ];
    }, $matches);
  }
  $ship = new Ship();
  foreach ($actions as $action) {
    $ship->action($action['name'], $action['value']);
  }
  $dist = abs($ship->x) + abs($ship->y);
  println('Part1 - Manhattan distance : %s.', $dist);

  $ship = new Ship();
  foreach ($actions as $action) {
    $ship->actionWithWaypoint($action['name'], $action['value']);
  }
  $dist = abs($ship->x) + abs($ship->y);
  println('Part2 - Manhattan distance : %s.', $dist);
}