<?php

//main(__DIR__ . '/example.txt');
main(__DIR__ . '/input.txt');

function main(string $filepath) {
  println('Input : ' . $filepath);
  $handle = fopen($filepath, 'rb');
  if (!$handle) {
    println('Cannot open file.');
    return;
  }
  $fields = [];
  $myTicket = NULL;
  $nearbyTickets = [];
  $filePart = 'fields';
  while (($line = fgets($handle)) !== FALSE) {
    $line = trim($line);
    switch ($filePart) {
      case 'fields':
        if (empty($line)) {
          $filePart = 'tickets';
        }
        elseif (preg_match('/^([\sa-z]+): (\d+)-(\d+) or (\d+)-(\d+)/', $line, $match)) {
          $fields[$match[1]] = [
            'min' => [(int) $match[2], (int) $match[4]],
            'max' => [(int) $match[3], (int) $match[5]],
          ];
        }
        break;
      case 'tickets':
        if (preg_match('/^\d/', $line)) {
          if (!isset($myTicket)) {
            $myTicket = array_map('intval', explode(',', $line));
          }
          else {
            $nearbyTickets[] = array_map('intval', explode(',', $line));
          }
        }
        break;
    }
  }
  fclose($handle);
  
  $sumInvalidValues = 0;
  foreach ($nearbyTickets as $ticket) {
    foreach ($ticket as $value) {
      $isValid = FALSE;
      foreach ($fields as $field => $limits) {
        $verifyRule = ($value >= $limits['min'][0] && $value <= $limits['max'][0]) || ($value >= $limits['min'][1] && $value <= $limits['max'][1]);
        if ($verifyRule) {
          $isValid = TRUE;
          break;
        }
      }
      if (!$isValid) {
        $sumInvalidValues += $value;
      }
    }
  }
  println('Part1 - Total nombres invalides : %s', $sumInvalidValues);
  
  // PART2
  $validFields = [];
  $nbValid = 0;
  foreach ($nearbyTickets as $id => $ticket) {
    $ticketValid = TRUE;
    $ticketFields = [];
    foreach ($ticket as $index => $value) {
      $isValid = FALSE;
      foreach ($fields as $field => $limits) {
        $verifyRule = ($value >= $limits['min'][0] && $value <= $limits['max'][0]) || ($value >= $limits['min'][1] && $value <= $limits['max'][1]);
        if ($verifyRule) {
          $ticketFields[$field][$index] = 1;
          $isValid = TRUE;
        }
      }
      if (!$isValid) {
        $ticketValid = FALSE;
        continue;
      }
    }
    if ($ticketValid) {
      $nbValid++;
      foreach ($ticketFields as $field => $indexes) {
        foreach ($indexes as $index => $a) {
          $validFields[$field][$index][$id] = 1;
        }
      }
    }
  }
  foreach ($validFields as $field => $indexes) {
    $validFields[$field] = array_map(function($ids) {
      return count($ids);
    }, $indexes);
    $validFields[$field] = array_filter($validFields[$field], function($ids) use ($nbValid) {
      return $ids >= 190;
    });
  }
  uasort($validFields, function ($a, $b) {
    return count($a) < count($b) ? -1 : 1;
  });
  $fields = [];
  while (!empty($validFields)) {
    $field = key($validFields);
    $indexes = array_keys(array_shift($validFields));
    $diff = array_diff($indexes, $fields);
    $index = array_shift($diff);
    $fields[$field] = $index;
  }
  asort($fields);
  $total = 1;
  foreach ($fields as $field => $index) {
    if (strpos($field, 'departure') === 0) {
      $total *= $myTicket[$index];
    }
  }
  println('Part2 : %s', $total);
}
