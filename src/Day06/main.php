<?php

main(__DIR__ . '/example.txt');
main(__DIR__ . '/input.txt');

function main(string $filepath) {
  println('Input : ' . $filepath);
  $input = file_get_contents($filepath);
  $lines = preg_split('/\r\n|\n/', $input);

  // PART 1
  $groups = [];
  $index = 0;
  foreach ($lines as $line) {
    $line = trim($line);
    if ($line === '') {
      $index++;
      continue;
    }
    if (!isset($groups[$index])) {
      $groups[$index] = [];
    }
    $groups[$index] += array_flip(str_split($line));
  }
  $sum = array_reduce($groups, function($total, $questions) {
    return $total + count($questions);
  }, 0);
  println(sprintf('Part1 - Somme des questions : %s', $sum));

  // PART 2
  $groups = [];
  $index = 0;
  foreach ($lines as $line) {
    $line = trim($line);
    if ($line === '') {
      $index++;
      continue;
    }
    $groups[$index][] = str_split($line);
  }
  foreach ($groups as $index => $questions) {
    if (count($questions) === 1) {
      $groups[$index] = $questions[0];
    }
    else {
      $groups[$index] = array_intersect(...$questions);
    }
  }
  $sum = array_reduce($groups, function($total, $questions) {
    return $total + count($questions);
  }, 0);
  println(sprintf('Part2 - Somme des questions : %s', $sum));
}