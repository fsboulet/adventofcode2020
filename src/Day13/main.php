<?php

main(__DIR__ . '/example.txt');
main(__DIR__ . '/input.txt');

function main(string $filepath) {
  println('Input : ' . $filepath);
  $input = file_get_contents($filepath);
  $busIds = [];
  $timestamp = 0;
  if (preg_match_all('/(\d+)/', $input, $matches)) {
    $ids = $matches[0];
    $timestamp = array_shift($ids);
    $busIds = array_map(function($id) {
      return (int) $id;
    }, $ids);
  }
  $timeToWait = NULL;
  $nextBus = NULL;
  foreach ($busIds as $busId) {
    $timeFromLastDepart = $timestamp % $busId;
    $wait = $busId - $timeFromLastDepart;
    if ($timeToWait === NULL || $wait < $timeToWait) {
      $timeToWait = $wait;
      $nextBus = $busId;
      if ($wait === 0) {
        break;
      }
    }
  }
  println('Part1 - Le prochain bus est le %s dans %s minutes. Bus ID x Minutes = %s', $nextBus, $timeToWait, ($nextBus * $timeToWait));
  
  $busIds = [];
  if (preg_match_all('/(\d+|x)/', $input, $matches)) {
    $ids = $matches[0];
    // Remove first line
    array_shift($ids);
    foreach ($ids as $offset => $id) {
      if ($id === 'x') {
        continue;
      }
      $busIds[] = [
        'id' => (int) $id,
        'offset' => $offset,
      ];
    }
  }

  /*
   * Code from https://www.reddit.com/r/adventofcode/comments/kc4njx/2020_day_13_solutions/gfpf97z/?utm_source=reddit&utm_medium=web2x&context=3
   */

  $t = 0;
  $cycle = reset($busIds)['id'];
  array_shift($busIds);
  foreach ($busIds as $bus) {
    $offset = $bus['offset'];
    $id = $bus['id'];
    while (($t + $offset) % $id !== 0) {
      $t += $cycle;
    }
    $cycle *= $id;
  }
  println('Part2 - Timestamp : %s', $t);
}
