<?php

main(__DIR__ . '/example.txt');
main(__DIR__ . '/input.txt');

function main(string $filepath) {
  $input = file_get_contents($filepath);
  $countValidPart1 = 0;
  $countValidPart2 = 0;
  if (preg_match_all('/^(\d+)-(\d+)\s([a-z]):\s([a-z]+)\s*$/m', $input, $matches, PREG_SET_ORDER) !== FALSE) {
    var_dump(count($matches));
    foreach ($matches as $match) {
      if (validate_part1($match)) {
        $countValidPart1++;
      }
      if (validate_part2($match)) {
        $countValidPart2++;
      }
    }
  }
  println("Part 1 : Nombre de mot de passe valide : " . $countValidPart1);
  println("Part 2 : Nombre de mot de passe valide : " . $countValidPart2);
}

function validate_part1(array $match): bool {
  list(, $min, $max, $character, $password) = $match;
  $regex = '/(' . $character . ')/';
  $count = preg_match_all($regex, $password);
  return (int)$min <= $count && $count <= (int)$max;
}

function validate_part2(array $match): bool {
  list(, $pos1, $pos2, $character, $password) = $match;
  $character1 = $password[--$pos1] ?? '';
  $character2 = $password[--$pos2] ?? '';
  return 
    ($character1 === $character && $character2 !== $character)
    ||
    ($character1 !== $character && $character2 === $character);
}