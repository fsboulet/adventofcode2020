<?php

use Day09\XMAS;

main(__DIR__ . '/example.txt', 5);
main(__DIR__ . '/input.txt', 25);

function main(string $filepath, int $preamble) {
  println('Input : ' . $filepath);
  $input = file_get_contents($filepath);
  $xmas = new XMAS($preamble);
  $xmas->load($input);
  if (!$xmas->checkValidity()) {
    println('Part1 - Première valeur non valide : %s', $xmas->currentNumber());
    $set = $xmas->searchSetOfNumbers($xmas->currentNumber());
    if (isset($set) && count($set) > 1) {
      sort($set);
      $weakness = reset($set) + end($set);
      println('Part2 - XMAS encryption weakness : %s', $weakness);
    }
    else {
      println('Part2 - XMAS encryption weakness not found.');
    }
  }
  else {
    println('Part1 - Les données XMAS sont valides');
  }
}