<?php

namespace Day09;

class XMAS {

  public int $preamble;
  public int $index;
  public array $numbers;
  public array $previousNumbers;

  public function __construct(int $preamble) {
    $this->preamble = $preamble;
    $this->index = $preamble;
  }

  public function load(string $input): void {
    $this->numbers = [];
    if (preg_match_all('/^([\d]+)/m', $input, $matches, PREG_SET_ORDER)) {
      $this->numbers = array_map(function($match) {
        return (int) $match[1];
      }, $matches);
    }
    $this->previousNumbers = array_slice($this->numbers, 0, $this->preamble);
  }

  public function currentNumber() {
    return $this->numbers[$this->index] ?? NULL;
  }

  public function checkValidity(): bool {
    $isValid = FALSE;

    while (!$this->isEnd() && $this->currentNumberIsValid()) {
      array_shift($this->previousNumbers);
      $this->previousNumbers[] = $this->currentNumber();
      $this->index++;
    }

    return $isValid;
  }

  public function isEnd(): bool {
    return $this->currentNumber() === NULL;
  }

  public function currentNumberIsValid(): bool {
    if ($this->isEnd()) {
      return FALSE;
    }

    $number = $this->currentNumber();
    foreach ($this->previousNumbers as $i => $number1) {
      for ($j = $i + 1; $j < $this->preamble; $j++) {
        $number2 = $this->previousNumbers[$j];
        if ($number === ($number1 + $number2)) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  public function searchSetOfNumbers(int $total) {
    $numbers = $this->numbers;
    $set = NULL;
    while ($set === NULL && !empty($numbers)) {
      $sum = 0;
      $index = 0;
      while (isset($numbers[$index])) {
        $sum += $numbers[$index];
        if ($sum > $total) {
          break;
        }
  
        if ($sum === $total) {
          $set = array_slice($numbers, 0, $index + 1);
          break 2;
        }
        $index++;
      }
      array_shift($numbers);
    }

    return $set;
  }

}