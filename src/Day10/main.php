<?php

main(__DIR__ . '/example2.txt');
main(__DIR__ . '/input.txt');

function main(string $filepath) {
  println('Input : ' . $filepath);
  $input = file_get_contents($filepath);
  $adapters = [];
  if (preg_match_all('/^([\d]+)/m', $input, $matches, PREG_SET_ORDER)) {
    $adapters = array_map(function($match) {
      return (int) $match[1];
    }, $matches);
  }
  if (empty($adapters)) {
    println('Part1 - Adapters are empty.');
    return;
  }
  sort($adapters);
  // Ajout prise de courant
  array_unshift($adapters, 0);
  $device = end($adapters) + 3;
  $adapters[] = $device;

  // PART 1
  $diffs = [
    0 => 0,
    1 => 0,
    2 => 0,
    3 => 0,
  ];
  foreach ($adapters as $index => $adapter) {
    if ($adapter === $device) {
      continue;
    }
    $next = $adapters[$index + 1];
    $diff = $next - $adapter;
    if ($diff > 3) {
      println(sprintf('Part1 - Il y a une trop grand différence entre les adaptateur : %s.', $diff));
      return;
    }
    $diffs[$diff]++;
  }
  println(sprintf('Part1 - 1-jolt (%s) x 3-jolt (%s) : %s.', $diffs[1], $diffs[3], $diffs[1] * $diffs[3]));

//  $arrangements = 0;
//  $paths = [];
//  foreach ($adapters as $adapter) {
//    if (!isset($paths[$adapter])) {
//      $paths[$adapter] = [
//        'count' => 0,
//        'exist' => FALSE,
//      ];
//    }
//    $paths[$adapter]['exist'] = TRUE;
//    for ($i = 1; $i <= 3; $i++) {
//      if (!isset($paths[$adapter + $i])) {
//        $paths[$adapter + $i] = [
//          'count' => 0,
//          'exist' => FALSE,
//        ];
//      }
//      $paths[$adapter + $i]['count']++;
//    }
//  }
//  $paths = array_filter($paths, function($path) {
//    return $path['exist'];
//  });
//  $paths = array_map(function($path) {
//    return $path['count'];
//  }, $paths);
//
//  $countOnePaths = [
//    1 => 0,
//    2 => 0,
//    3 => 0,
//  ];
//  $countFollowingOne = 0;
//  foreach ($paths as $adapter => $path) {
//    if ($countFollowingOne === 3 || $path !== 1) {
//      $countOnePaths[$countFollowingOne]++;
//      $countFollowingOne = 0;
//    }
//    if ($path === 1) {
//      $countFollowingOne++;
//    }
//  }
//  $arrangements = 0;
//  foreach ($countOnePaths as $length => $count) {
//    if ($count > 0 && $length > 0) {
//      $arrangements += pow($length, $count);
//    }
//  }
//  // var_dump($countOnePaths);
//  var_dump($arrangements);
//  // println(sprintf('Part2 - Nombre d\'arrangements : %s.', $arrangements));

}

//class Node {
//
//  public array $children;
//  public int $adapter;
//
//  public function __construct(int $adapter) {
//    $this->adapter = $adapter;
//    $this->children = [];
//  }
//
//  public function addChild(Node $child) {
//    $this->children[] = $child;
//  }
//
//}