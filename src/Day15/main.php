<?php

use Day15\MemoryGame;

//main('0,3,6');
//main('1,3,2');
//main('2,1,3');
//main('1,2,3');
//main('2,3,1');
//main('3,2,1');
//main('3,1,2');
main('13,16,0,12,15,1');

function main(string $input) {
  $numbers = explode(',', $input);
  $game = new MemoryGame($numbers);
  while ($game->turn < 2020) {
    $game->speak();
  }
  println('Part1 - Numéro au tour 2020 : %s', $game->next);
  
  while ($game->turn < 30000000) {
    $game->speak();
  }
  println('Part1 - Numéro au tour 30000000 : %s', $game->next);
}
