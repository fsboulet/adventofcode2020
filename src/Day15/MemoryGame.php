<?php

namespace Day15;

class MemoryGame {
  
  public array $memory;
  public int $turn;
  public int $next;
  
  public function __construct(array $starting_numbers) {
    $this->memory = [];
    $this->turn = 1;
    $this->next = -1;
    foreach ($starting_numbers as $number) {
      $this->setNumber($number);
    }
  }
  
  public function setNumber(int $number): void {
      if (!isset($this->memory[$number])) {
        $this->next = 0;
      }
      else {
        $this->next = $this->turn - $this->memory[$number];
      }
      $this->memory[$number] = $this->turn++;
  }
  
  public function speak(): void {
    $this->setNumber($this->next);
  }
  
}