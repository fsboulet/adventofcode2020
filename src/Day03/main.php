<?php

main(__DIR__ . '/example.txt');
main(__DIR__ . '/input.txt');

function main(string $filepath) {
  $input = file_get_contents($filepath);
  $map = new \Day03\Map($input);
  $params = [
    [1, 1],
    [3, 1],
    [5, 1],
    [7, 1],
    [1, 2],
  ];
  $count = [];
  foreach ($params as $param) {
    list($x, $y) = $param;
    $key = $x . ':' . $y;
    $count[$key] = 0;
    $map->reset();
    while (!$map->isArrived()) {
      $map->move($x, $y);
      if ($map->isTree()) {
        $count[$key]++;
      }
    }
    println(sprintf('Nombre d\'arbres (%s, %s) : %s', $x, $y, $count[$key]));
  }
  $total = array_reduce($count, function($carry, $item) {
    if (!isset($carry)) {
      $carry = $item;
    }
    else {
      $carry *= $item;
    }
    return $carry;
  });
  println(sprintf('Multiplication du nombre d\'arbres : %s', $total));
}