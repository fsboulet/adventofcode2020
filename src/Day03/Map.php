<?php

namespace Day03;

class Map {

  public const TREE = '#';

  public $width;
  public int $height;
  public array $arr;
  public int $x;
  public int $y;

  public function __construct(string $input) {
    $this->width = 0;
    $this->height = 0;
    $this->arr = [];
    $this->x = 0;
    $this->y = 0;

    $lines = preg_split('/\r\n|\n/', $input);
    foreach ($lines as $index => $line) {
      $boxes = str_split(trim($line));
      if ($index === 0) {
        $this->width = count($boxes);
      }
      $this->arr[$this->height] = $boxes;
      $this->height++;
    }
  }

  public function reset(): void {
    $this->x = 0;
    $this->y = 0;
  }

  public function move($x, $y): void {
    $this->x = ($this->x + $x) % $this->width;
    $this->y += $y;
  }

  public function getBox(): string {
    return $this->arr[$this->y][$this->x] ?? '';
  }

  public function isTree(): bool {
    return $this->getBox() === self::TREE;
  }

  public function isArrived(): bool {
    return $this->y >= $this->height;
  }

}