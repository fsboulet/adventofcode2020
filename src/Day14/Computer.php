<?php

namespace Day14;

class Computer {
  
  public const BITMASK_LENGTH = 36;
  
  public int $and_bitmask;
  public int $or_bitmask;
  public int $x_bitmask;
  public array $addr_bitmasks;
  public array $memory;
  
  public function __construct() {
    $this->memory = [];
  }
  
  public function command(array $params, bool $part2 = FALSE) {
    $name = array_shift($params);
    if ($name === 'mask') {
      $this->setBitmask($params[0]);
    }
    else if (!$part2) {
      $this->write((int) $params[0], (int) $params[1]);
    }
    else {
      $this->writePart2((int) $params[0], (int) $params[1]);
    }
  }
  
  public function setBitmask(string $mask) {
    $this->and_bitmask = bindec(str_replace('X', '1', $mask));
    $this->or_bitmask = bindec(str_replace('X', '0', $mask));
  }
  
  public function write(int $address, int $value) {
    $this->memory[$address] = $this->applyBitmask($value);
  }
  
  public function applyBitmask(int $value): int {
    assert(isset($this->or_bitmask, $this->and_bitmask));
    $value &= $this->and_bitmask;
    $value |= $this->or_bitmask;
    return $value;
  }
  
  public function commandPart2(array $params) {
    $name = array_shift($params);
    if ($name === 'mask') {
      $this->setBitmaskPart2($params[0]);
    }
    else {
      $this->writePart2((int) $params[0], (int) $params[1]);
    }
  }
  
  public function setBitmaskPart2(string $mask) {
    $offsets = [];
    if (preg_match_all('/(X)/', $mask, $matches, PREG_OFFSET_CAPTURE)) {
      $offsets = array_map(function ($match) {
        return $match[1];
      }, $matches[1]);
    }
    $this->x_bitmask = bindec(str_replace(['0', 'X'], ['1', '0'], $mask));
    $this->addr_bitmasks = [bindec(str_replace('X', '0', $mask))];
    foreach ($offsets as $offset) {
      $bitmask = 2 ** (self::BITMASK_LENGTH - $offset - 1);
      foreach ($this->addr_bitmasks as $addr_bitmask) {
        $this->addr_bitmasks[] = $bitmask | $addr_bitmask;
      }
    }
  }
  
  public function writePart2(int $address, int $value) {
    assert(isset($this->addr_bitmasks, $this->x_bitmask));
    // Replace all bits at X position with 0
    $address &= $this->x_bitmask;
    foreach ($this->addr_bitmasks as $addr_bitmask) {
      $memory_address = $address | $addr_bitmask;
      $this->memory[$memory_address] = $value;
    }
  }
  
  public function printBitmasks(array $bitmasks) {
    foreach ($bitmasks as $bitmask) {
      $binary_string = str_pad(decbin($bitmask), self::BITMASK_LENGTH, '0', STR_PAD_LEFT);
      println($binary_string);
    }
  }
  
}