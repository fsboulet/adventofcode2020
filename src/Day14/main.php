<?php

use Day14\Computer;

main(__DIR__ . '/example.txt');
main(__DIR__ . '/example2.txt', TRUE);
main(__DIR__ . '/input.txt', TRUE);

function main(string $filepath, bool $part2 = FALSE) {
  println('Input : ' . $filepath);
  $input = file_get_contents($filepath);
  $commands = [];
  if (preg_match_all('/^(?:(mask) = ([01X]+)|(mem)\[([\d]+)\] = ([\d]+))\s*$/m', $input, $matches, PREG_UNMATCHED_AS_NULL)) {
    $length = count($matches);
    for ($i = 1; $i < $length; $i++) {
      foreach ($matches[$i] as $index => $match) {
        if ($match === NULL) {
          continue;
        }
        $commands[$index][] = $match;
      }
    }
    ksort($commands);
  }
  
  if (!$part2) {
    // PART1
    $computer = new Computer();
    foreach ($commands as $command) {
      $computer->command($command);
    }
    $sum = array_reduce($computer->memory, function ($total, $value) {
      return $total + $value;
    }, 0);
    println('Part1 - Somme des valeurs en mémoire : %s', $sum);
  }
  else {
    // PART2
    $computer = new Computer();
    foreach ($commands as $command) {
      $computer->commandPart2($command);
    }
    $sum = array_reduce($computer->memory, function ($total, $value) {
      return $total + $value;
    }, 0);
    println('Part2 - Somme des valeurs en mémoire : %s', $sum);
  }
}
