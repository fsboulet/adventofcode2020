<?php

use Day11\Map;

main(__DIR__ . '/example.txt');
main(__DIR__ . '/input.txt');

function main(string $filepath) {
  println('Input : ' . $filepath);
  $input = file_get_contents($filepath);
  $map = new Map($input);
//  $map->print();
  $stabilize = FALSE;
  $lastState = json_encode($map->arr);
  while (!$stabilize) {
    $map->nextRound();
//    $map->print();
    $newState = json_encode($map->arr);
    if ($newState === $lastState) {
      $stabilize = TRUE;
    }
    $lastState = $newState;
  }
  println(sprintf('Part1 - Nombre de places occupées : %s.', $map->countOccupiedSeats()));
}