<?php

namespace Day11;

class Map {

  public const FLOOR = '.';
  public const SEAT = 'L';
  public const PERSON = '#';
  public const DIRECTIONS = ['NW', 'N', 'NE', 'E', 'SE', 'S', 'SW', 'W'];

  public int $width;
  public int $height;
  public array $arr;
  public int $x;
  public int $y;

  public function __construct(string $input) {
    $this->width = 0;
    $this->height = 0;
    $this->arr = [];
    $this->x = 0;
    $this->y = 0;

    $lines = preg_split('/\r\n|\n/', $input);
    foreach ($lines as $index => $line) {
      $boxes = str_split(trim($line));
      if ($index === 0) {
        $this->width = count($boxes);
      }
      $this->arr[$this->height] = $boxes;
      $this->height++;
    }
  }

  public function nextRound(): void {
    $newArr = [];
    $this->reset();
    while (!$this->isEnd()) {
      $newArr[$this->y][$this->x] = $this->applyRules();
      $this->nextBox();
    }
    $this->arr = $newArr;
  }

  public function reset(): void {
    $this->x = 0;
    $this->y = 0;
  }

  public function nextBox(): void {
    $this->x = ($this->x + 1) % $this->width;
    if ($this->x === 0) {
      $this->y++;
    }
  }

  public function getBox() {
    return $this->arr[$this->y][$this->x] ?? NULL;
  }

  public function isEnd(): bool {
    return $this->getBox() === NULL;
  }

  public function applyRules(): string {
    $box = $this->getBox();
    if ($box === self::FLOOR) {
      return $box;
    }
    // PART 1
    // $occupiedAdjacentSeats = 0;
    // for ($x = ($this->x - 1); $x <= ($this->x + 1); $x++) {
    //   if ($x < 0 || $x >= $this->width) {
    //     continue;
    //   }
    //   for ($y = ($this->y - 1); $y <= ($this->y + 1); $y++) {
    //     if ($y < 0 || $y >= $this->height) {
    //       continue;
    //     }
    //     if ($x === $this->x && $y === $this->y) {
    //       continue;
    //     }
    //     $adjacentBox = $this->arr[$y][$x] ?? NULL;
    //     if ($adjacentBox === NULL) {
    //       continue;
    //     }
    //     if ($adjacentBox === self::PERSON) {
    //       $occupiedAdjacentSeats++;
    //     }
    //     if (
    //       ($box === self::SEAT && $occupiedAdjacentSeats > 0) ||
    //       ($box === self::PERSON && $occupiedAdjacentSeats >= 4)
    //     ) {
    //       return self::SEAT;
    //     }
    //   }
    // }

    // PART 2
    $occupiedAdjacentSeats = 0;
    foreach (self::DIRECTIONS as $direction) {
      $occupiedAdjacentSeats += $this->isSeatOccupied($this->x, $this->y, $direction) ? 1 : 0;
      if (
        ($box === self::SEAT && $occupiedAdjacentSeats > 0) ||
        ($box === self::PERSON && $occupiedAdjacentSeats >= 5)
      ) {
        return self::SEAT;
      }
    }

    return self::PERSON;
  }

  public function isSeatOccupied($x, $y, $direction, $first = TRUE): ?bool {
    $box = $this->arr[$y][$x] ?? NULL;
    if ($box === NULL) {
      return FALSE;
    }
    
    if ($box === self::FLOOR || $first) {
      switch ($direction) {
        case 'NW':
          $x--;
          $y--;
          break;
        case 'N':
          $y--;
          break;
        case 'NE':
          $x++;
          $y--;
        break;
        case 'E':
          $x++;
          break;
        case 'SE':
          $x++;
          $y++;
          break;
        case 'S':
          $y++;
          break;
        case 'SW':
          $x--;
          $y++;
          break;
        case 'W':
          $x--;
          break;
      }
      return $this->isSeatOccupied($x, $y, $direction, FALSE);
    }
  
    if ($box === self::PERSON) {
      return TRUE;
    }
    
    return FALSE;
  }

  public function countOccupiedSeats(): int {
    $this->reset();
    $count = 0;
    while (!$this->isEnd()) {
      if ($this->getBox() === self::PERSON) {
        $count++;
      }
      $this->nextBox();
    }

    return $count;
  }

  public function print(): void {
    for ($y = 0; $y < $this->height; $y++) {
      println(implode('', $this->arr[$y]));
    }
    println('');
  }

}