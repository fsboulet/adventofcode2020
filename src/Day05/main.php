<?php

main(__DIR__ . '/example.txt');
main(__DIR__ . '/input.txt');

function main(string $filepath) {
  println('Input : ' . $filepath);
  $input = file_get_contents($filepath);
  $lines = preg_split('/\r\n|\n/', $input);
  $maxId = 0;
  $ids = [];
  foreach ($lines as $line) {
    $row = get_row(substr($line, 0, 7));
    $column = get_column(substr($line, -3, 3));
    $id = $row * 8 + $column;
    if ($id > $maxId) {
      $maxId = $id;
    }
    if ($row > 0 && $row < 127) {
      $ids[] = $id;
    }
  }
  sort($ids);

  $seatId = NULL;
  foreach ($ids as $index => $id) {
    if (!isset($ids[$index + 1])) {
      continue;
    }
    if (($ids[$index + 1] - $id) === 2) {
      $seatId = $id + 1;
      break;
    }
  }
  println(sprintf('Part1 - Highest seat ID : %s', $maxId));
  println(sprintf('Part2 - seat ID : %s', $seatId));
}

function get_row($input) {
  return get_value($input, 0, 127);
}

function get_column($input) {
  return get_value($input, 0, 7);
}

function get_value($input, $min, $max) {
  if ($min === $max) {
    return $min;
  }
  $char = $input[0];
  if ($char === 'F' || $char === 'L') {
    $max = (int) floor(($min + $max) / 2);
  }
  else {
    $min = (int) ceil(($min + $max) / 2);
  }
  return get_value(substr($input, 1), $min, $max);
}