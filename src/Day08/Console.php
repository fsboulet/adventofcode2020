<?php

namespace Day08;

class Console {

  public array $instructions;
  public int $pos;
  public int $acc;
  public array $executedPos = [];

  public function __construct() {
    $this->reset();
  }

  public function load(string $program): void {
    $this->reset();
    $this->instructions = [];
    if (preg_match_all('/^([a-z]+)\s([+|\-]\d+)/m', $program, $matches, PREG_SET_ORDER)) {
      $this->instructions = array_map(function($match) {
        return new Instruction($match[1], $match[2]);
      }, $matches);
    }
  }

  public function reset(): void {
    $this->pos = 0;
    $this->acc = 0;
    $this->executedPos = [];
  }

  public function run(): void {
    while (!$this->isEnd()) {
      if ($this->infiniteLoop()) {
        break;
      }
      $this->executedPos[$this->pos] = TRUE;
      $instruction = $this->instructions[$this->pos];
      $this->acc += $instruction->getValue();
      $this->pos += $instruction->getJump();
    }
  }

  public function infiniteLoop(): bool {
    return isset($this->executedPos[$this->pos]);
  }

  public function isEnd(): bool {
    return !isset($this->instructions[$this->pos]);
  }

  public function tryFixInfiniteLoop(int $pos): int {
    $length = count($this->instructions);
    while ($pos < $length) {
      $instruction = $this->instructions[$pos];
      $newOp = NULL;
      if ($instruction->op === 'jmp') {
        $newOp = 'nop';
      }
      elseif ($instruction->op === 'nop') {
        $newOp = 'jmp';
      }
      if (isset($newOp)) {
        $this->instructions[$pos] = clone $instruction;
        $this->instructions[$pos]->op = $newOp;
        break;
      }
      $pos++;
    }

    return $pos;
  }

}