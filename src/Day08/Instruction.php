<?php

namespace Day08;

class Instruction {

  public string $op;
  public int $arg;

  public function __construct(string $op, string $arg) {
    $this->op = $op;
    $this->arg = (int) $arg;
  }

  public function toString(): string {
    return $this->op . ' ' . $this->arg;
  }

  public function getValue(): int {
    return $this->op === 'acc' ? $this->arg : 0;
  }

  public function getJump(): int {
    return $this->op === 'jmp' ? $this->arg : 1;
  }

}