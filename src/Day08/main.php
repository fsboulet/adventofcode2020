<?php

use Day08\Console;

main(__DIR__ . '/example.txt');
main(__DIR__ . '/input.txt');

function main(string $filepath) {
  println('Input : ' . $filepath);
  $input = file_get_contents($filepath);
  $console = new Console();
  $console->load($input);
  $console->run();
  $accValue = NULL;
  if (!$console->isEnd() && $console->infiniteLoop()) {
    $accValue = $console->acc;
  }
  println(sprintf('Part1 - Valeur de l\'accumulateur avant la boucle infinie : %s', $accValue));

  $pos = 0;
  $backupInstructions = $console->instructions;
  while(!$console->isEnd() && $console->infiniteLoop()) {
    $console->reset();
    $console->instructions = $backupInstructions;
    $pos = $console->tryFixInfiniteLoop($pos) + 1;
    $console->run();
  }
  println(sprintf('Part1 - Valeur de l\'accumulateur à la fin du programme : %s', $console->acc));
}