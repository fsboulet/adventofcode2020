<?php

main(__DIR__ . '/example.txt');
main(__DIR__ . '/input.txt');

function main(string $filepath) {
  println('Input : ' . $filepath);
  $input = file_get_contents($filepath);
  $bagKey = 'shiny gold';
  $total = 0;
  $bags = [];
  // Suppression des lignes contenant "no other"
  $input = preg_replace('/(\n?[^\n]+no\sother[^n]*\n)/', "\n", $input);
  if (preg_match_all('/(?:(?:^|([0-9]+)\s)([a-z]+)\s([a-z]+)\sbags?)/m', $input, $matches, PREG_SET_ORDER)) {
    $index = NULL;
    foreach ($matches as $match) {
      list(, $count, $adjective, $color) = $match;
      // Si $count est vide, c'est le début de la règle
      if ($count === '') {
        $index = $adjective . ' ' . $color;
        continue;
      }
      if ($index === NULL) {
        continue;
      }
      $bags[$index][$adjective . ' ' . $color] = $count;
    }
  }
  
  foreach ($bags as $index => $count) {
    $bagKeys = get_all_bag_keys($index, $bags);
    if (bag_contain($bagKey, $index, $bags)) {
      $total++;
    }
  }
  println(sprintf('Part1 - Nombre de sacs qui peuvent contenir un sac "%s" : %s', $bagKey, $total));
  println(sprintf('Part1 - Nombre de sacs que doit contenir un sac "%s" : %s', $bagKey, count_bags($bagKey, $bags)));
}

function bag_contain($search, $index, &$all_bags) {
  if (isset($all_bags[$index][$search])) {
    return TRUE;
  }

  if (!empty($all_bags[$index])) {
    foreach ($all_bags[$index] as $sub_key => $count) {
      if (bag_contain($search, $sub_key, $all_bags)) {
        return TRUE;
      }
    }
  }

  return FALSE;
}

function get_all_bag_keys($index, &$all_bags) {
  static $cache = [];

  if (!isset($cache[$index]) || TRUE) {
    $cache[$index] = [];
    if (isset($all_bags[$index])) {
      foreach ($all_bags[$index] as $sub_index => $count) {
        $cache[$index][$sub_index] = $count;
        $cache[$index] += get_all_bag_keys($sub_index, $all_bags);
      }
    }
  }

  return $cache[$index];
}

function count_bags($bag, &$all_bags) {
  if (!isset($all_bags[$bag])) {
    return 0;
  }

  $total = 0;
  foreach ($all_bags[$bag] as $sub_bag => $count) {
    $sub_count = count_bags($sub_bag, $all_bags);
    $total += $count + ($count * $sub_count);
  }

  return $total;
}