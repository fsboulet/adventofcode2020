<?php

main(__DIR__ . '/example.txt');
main(__DIR__ . '/invalid.txt');
main(__DIR__ . '/valid.txt');
main(__DIR__ . '/input.txt');

function main(string $filepath) {
  println('Input : ' . $filepath);
  $input = file_get_contents($filepath);
  $lines = preg_split('/\r\n|\n/', $input);
  $passports = [];
  $index = 0;
  foreach ($lines as $line) {
    $line = trim($line);
    if ($line === '') {
      $index++;
    }
    else {
      if (!isset($passports[$index])) {
        $passports[$index] = $line;
      }
      else {
        $passports[$index] .= ' ' . $line;
      }
    }
  }
  
  $countValidPart1 = 0;
  $countValidPart2 = 0;
  foreach ($passports as $passport) {
    if (validate_part1($passport)) {
      $countValidPart1++;
    }
    if (validate_part2($passport)) {
      $countValidPart2++;
    }
  }
  println(sprintf('Part1 - Nombre de passeports valide : %s / %s', $countValidPart1, count($passports)));
  println(sprintf('Part2 - Nombre de passeports valide : %s / %s', $countValidPart2, count($passports)));
}

function validate_part1($passport) {
  $requiredFields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid', 'cid'];
  if (preg_match_all('/([a-z]{3}):([#0-9a-zA-Z]+)+/', $passport, $matches)) {
    $fields = $matches[1] ?? [];
    $missingFields = array_values(array_diff($requiredFields, $fields));
    return empty($missingFields) || (count($missingFields) === 1 && $missingFields[0] === 'cid');
  }

  return FALSE;
}

function validate_part2($passport) {
  $requiredFields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid', 'cid'];
  if (preg_match_all('/([a-z]{3}):([#0-9a-zA-Z]+)+/', $passport, $matches)) {
    $fields = $matches[1] ?? [];
    $missingFields = array_values(array_diff($requiredFields, $fields));
    $validated = empty($missingFields) || (count($missingFields) === 1 && $missingFields[0] === 'cid');
    if ($validated) {
      foreach ($matches[1] as $index => $field) {
        $validated = $validated && validate_field($field, $matches[2][$index]);
      }
    }
    return $validated;
  }

  return FALSE;
}

function validate_field($field, $value) {
  $validated = FALSE;
  
  switch($field) {
    case 'byr':
      $validated = validate_date($value, 1920, 2002);
    break;
    case 'iyr':
      $validated = validate_date($value, 2010, 2020);
    break;
    case 'eyr':
      $validated = validate_date($value, 2020, 2030);
    break;
    case 'hgt':
      if (preg_match('/(\d+)(cm|in)/', $value, $match)) {
        $number = (int) $match[1];
        if ($match[2] === 'cm') {
          $min = 150;
          $max = 193;
        }
        else {
          $min = 59;
          $max = 76;
        }
        $validated = $min <= $number && $number <= $max;
      }
    break;
    case 'hcl':
      $validated = preg_match('/^#[0-9a-f]{6}$/', $value) === 1;
    break;
    case 'ecl':
      $validated = preg_match('/^amb|blu|brn|gry|grn|hzl|oth$/', $value) === 1;
    break;
    case 'pid':
      $validated = preg_match('/^\d{9}$/', $value) === 1;
    break;
    case 'cid':
      $validated = TRUE;
    break;
  }
  
  return $validated;
}

function validate_date($value, $min, $max) {
  if (preg_match('/\d{4}/', $value)) {
    $value = (int) $value;
    return $min <= $value && $value <= $max;
  }
  return FALSE;
}