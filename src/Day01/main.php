<?php

main();

function main() {
  $input = file_get_contents(__DIR__ . './input.txt');
  $numbers = preg_split('/\r\n|\n/', $input);
  $numbers = array_map(function($number) {
    return (int) $number;
  }, $numbers);
  sort($numbers);

  $total = 2020;
  $result = NULL;
  // STEP 1
  // foreach ($numbers as $i => $number) {
  //   foreach ($numbers as $j => $number2) {
  //     if ($i === $j) {
  //       continue;
  //     }
  //     $sum = $number + $number2;
  //     if ($sum === $total) {
  //       $result = $number * $number2;
  //       break 2;
  //     }
  //     if ($sum > $total) {
  //       break;
  //     }
  //   }
  // }

  // STEP 2
  foreach ($numbers as $i => $number) {
    foreach ($numbers as $j => $number2) {
      if ($i === $j) {
        continue;
      }
      if ($number + $number2 >= $total) {
        break;
      }
      foreach ($numbers as $k => $number3) {
        if ($k === $i || $k === $j) {
          continue;
        }
        $sum = $number + $number2 + $number3;
        if ($sum === $total) {
          $result = $number * $number2 * $number3;
          break 3;
        }
        if ($sum > $total) {
          break;
        }
      }
    }
  }
  println('Résultat : ' . $result);
}
