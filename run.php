<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/helper.php';

Kint::$mode_default_cli = Kint::MODE_TEXT;

$cmd = new Commando\Command();

$cmd
  ->option()
  ->require()
  ->describedAs('Day number')
  ->must(function ($day) {
    return file_exists(__DIR__ . '/src/Day' . $day . '/main.php');
  });

$day = $cmd[0];
@d(microtime());
require_once __DIR__ . '/src/Day' . $day . '/main.php';
d(microtime());